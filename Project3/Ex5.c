#include <stdio.h>
#include <assert.h>
typedef struct node {
	int num;
	struct node *next;
} Node;


Node* build_numbers_list();
Node* createList2(Node** head);
void print_number_list(Node* lst);
Node* Delete(Node** head, Node* pos);
void Insert(Node** head, Node* node);

void main() {


	Node* number_list = build_numbers_list();
	printf("The Original Numbers are:\n");
	print_number_list(number_list);
	Node* odd_numbers_list = createList(&number_list);
	printf("\nThe Odd Numbers list are:\n");
	print_number_list(odd_numbers_list);
	printf("\nThe Even Numbers list are:\n");
	print_number_list(number_list);
	getch();

}

void print_number_list(Node* lst) {
	if (lst == NULL) {
		printf("---Empty---");
		return;
	}
	while (lst != NULL) {
		printf("%d ", lst->num);
		lst = lst->next;
	}
}

Node* build_numbers_list() {
	Node* head = NULL;
	Node* curr_node;
	Node* tmp;
	int number;
	printf("Please Enter Numbers to insert to the list(enter negative number to finish the scan):\n");
	// Handle the first element
	scanf("%d", &number);
	if (number < 0) return NULL;
	head = (Node*)malloc(sizeof(Node));
	assert(head);
	head->next = NULL;
	head->num = number;
	curr_node = head;
	//Handle the other elements
	scanf("%d", &number);
	while (number >= 0) {
		tmp = (Node*)malloc(sizeof(Node));
		tmp->next = NULL;
		tmp->num = number;
		assert(tmp);
		curr_node->next = tmp;
		curr_node = curr_node->next;

		scanf("%d", &number);
	}

	return head;
}
Node* Delete(Node** head, Node* pos) {
	/*
	Delete pos->next, in case pos is null delete the first element.
	*/
	Node *tmp;
	if (*head == NULL) return NULL; // Case of list is empty
	if (pos == NULL) {
		// Delete the first element
		tmp = *head;
		*head = tmp->next;
		return tmp;
	}
	else if (pos->next != NULL) {
		tmp = pos->next;
		pos->next = tmp->next;
		return tmp;
	}

}

void Insert(Node** head, Node* node) {
	/*
	Insert a node to the end of the linked list.
	*/
	// Case the list is empty
	if (*head == NULL) {
		*head = node;
	}
	// insert into the end of the linked list
	else {
		Node* tmp = *head;
		while (tmp->next != NULL) {
			tmp = tmp->next;
		}
		tmp->next = node;
	}

}

Node* createList(Node** head) {
	Node* odd_list = NULL;
	Node* prev = NULL;
	Node* current_node = *head;
	if (*head == NULL) return NULL;
	while (current_node != NULL) {
		int number = current_node->num;
		if (number % 2 == 0) {
			prev = current_node;
			current_node = current_node->next;
		}
		else {
			current_node = current_node->next;
			Node* tmp = Delete(head, prev);
			Insert(&odd_list, tmp);
		}
	}

	return odd_list;
}